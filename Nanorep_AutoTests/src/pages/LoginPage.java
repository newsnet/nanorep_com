package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import configuration.Ņonfiguration;
import context.Context;

public class LoginPage extends Page {
	   public static final String PAGE_URL = Context.getSiteUrl()+"/console/login.html";
	   protected LoginPage() {
	       super(PAGE_URL);
	   }
	   Robot robot;
	   
	   public static LoginPage openLoginPage() {
	       LoginPage loginPage = new LoginPage();
	       loginPage.getDriver().get(PAGE_URL);
	       return loginPage;
	   }
	   
	   public void sendQuestionInSearchBox(String questionText){		   
		   selectSearchBox(questionText); 		
	   }
	   
	   public void selectLastSuggestedQuestion(){
		 
		   try {
			   if(robot==null)
				   robot = new Robot();
			   robot.delay(3000);
			   robot.mouseMove((int)(950*Ņonfiguration.scale), 340); // Move the mouse on  coordinates x, y
			   robot.mousePress(InputEvent.BUTTON1_MASK); // click left button at mouse
			   robot.delay(300); // 300 wait
			   robot.mouseRelease(InputEvent.BUTTON1_MASK); // Release the left mouse button   
			   robot.delay(3000);			   
			   
			   
			   
			   
		   } catch (AWTException e) {
			
			   e.printStackTrace();
		   }
		  
	   }
	
	   public String getLastLike(){		
		   getDriver().findElement(By.xpath("//div[@class='video']")).click();//scrolling imitating 		
		   switchToIframe();
		   List<WebElement> likes = getDriver().findElements(By.xpath(".//div[contains(@class,'newlikes')]"));		  
		   return likes.get(likes.size()-1).getText();
		  
	   }
	   
	   private void switchToIframe(){
		  
		   getDriver().switchTo().defaultContent(); 		   
		   String idIframe = getDriver().findElement(By.xpath("//div[@class='guest']//iframe")).getAttribute("id");
		   getDriver().switchTo().frame(idIframe);	
		  
	   }
	   
	   private void selectSearchBox(String questionText){
		 
		   try {
			   if(robot==null)
				   robot = new Robot();
			   robot.delay(3000);
			   robot.mouseMove((int)(900*Ņonfiguration.scale), 235); 
			   robot.mousePress(InputEvent.BUTTON1_MASK); 
			   robot.delay(300); 
			   robot.mouseRelease(InputEvent.BUTTON1_MASK);	   
			   robot.delay(300);			   
			   
			   
			   sendKeys(questionText,robot);
			   
		   } catch (AWTException e) {
			
			   e.printStackTrace();
		   }
		   
		   
		   
		 
	   }
	   
	   private void sendKeys(String text, Robot robot){
		   
		  
		   for(int i=0; i<text.length();i++){
			  
			 
			   robot.keyPress(Character.toUpperCase(text.charAt(i)));
			   robot.keyRelease(text.charAt(i));
			   
		   }
	   }
	   
	   
	   
	  
	   protected void parsePage() {
	       
	   }
	   protected void init() {
	      
	   }
	  
	   
	}