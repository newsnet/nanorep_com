package context;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import configuration.Ņonfiguration;

public class Context {
	public static final String BROWSER_IE = "*iexplore";
    public static final String BROWSER_FF = "*firefox";
    public static final String BROWSER_CH = "*chrome";
    private static Context context;
    private static String siteUrl;
    
    private static WebDriver driver;
	
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

 
    private Context() {
    }
    public static void initInstance(String browserType, String siteURL) {
        context = new Context();
        siteUrl = siteURL;
        chooseBrouser(browserType);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        System.out.println("Size: " + driver.manage().window().getSize().height + " x " + driver.manage().window().getSize().width);
        Ņonfiguration.scale = driver.manage().window().getSize().width/(float)Ņonfiguration.WIDTH;
        
        System.out.println("Scale: " + Ņonfiguration.scale+"\n");
        driver.get(siteURL);
       
    }
    
    private static void chooseBrouser(String browserType){
    	switch (browserType) {
		case BROWSER_FF:
			driver = new FirefoxDriver();
			break;
		case BROWSER_IE:
			//driver = new FirefoxDriver();
			break;
		case BROWSER_CH:
			//driver = new FirefoxDriver();
			break;		
		}
    }
    
    public static Context getInstance() {
        if (context == null) {
            throw new IllegalStateException("Context is not initialized");
        }
        return context;
    }

    public static String getSiteUrl() {
    	return siteUrl;
    }
    
    public WebDriver getDriver() {
        if (driver != null) {
            return driver;
        }
        throw new IllegalStateException("WebDriver is not initialized");
    }
    
    
    
    public void close() {
    	driver.quit();
    }
    
   
}
