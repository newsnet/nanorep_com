package tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import configuration.Ņonfiguration;
import context.Context;

public class TestQuestionBox {
	
	@BeforeClass
	public void setUp() {       
        Context.initInstance(Context.BROWSER_FF, Ņonfiguration.SITE_URL);
    }
   
	
	
	@Test
	public void testLastNumberTheSuggestedAnswers(){
		String actualValue = "0";		
		String questionText = "how much";
		LoginPage loginPage = LoginPage.openLoginPage();				
		loginPage.sendQuestionInSearchBox(questionText);
		loginPage.selectLastSuggestedQuestion();
		String lastLike = loginPage.getLastLike();
		System.out.println("Return the last like/dislike number  in the suggested answers: " +lastLike);
		Assert.assertEquals(actualValue, lastLike," Incorrect value reviews like/dislike ");
	}
	
	
	
	
	
    
    @AfterClass
    protected void tearDown() throws Exception {        
       Context.getInstance().close();
    }
	
	
	
}
